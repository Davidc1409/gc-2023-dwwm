# gc-2023-dwwm
GameConnect est un site qui propose sous le format d'un blog de parler de l'actualité du jeux vidéo et
de l'univers vidéoludique. Le site permet également à ses visiteurs de discuter en vocal ou en
discussion instantanée comme Discord. L'utilisateur peut ajouter des amis dans sa liste d'amis, il peut
participer à des discussions privées dans lesquelles lui et ses amis auront créé au préalable.
Fonctionnalités :
Blog :

création d'articles
édition d'articles
publication d'articles
Discussion :
vocale
écrit

Langage, framework et environnement d'exécution utilisés : HTML5/CSS, Bootstrap 5.2, Javascript/Jquery, PHP, Socket.io, NodeJs, ExpressJs
